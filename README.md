<h1><img src="https://symbols-electrical.getvecta.com/stencil_73/34_adonisjs.afb87a2e32.svg" alt="AdonisJs logo" width=250 height=80></h1>

[[Proposta](https://gitlab.com/InfoJrUFBA/projetos/internos/Pluginfo/Trainee/2020-2/atividade-react/-/issues/2) | [Modelagem do DB](https://dbdiagram.io/d/5fdb863d9a6c525a03bb79ea) | [Documentação da API](https://documenter.getpostman.com/view/13907074/TVsrGVRm)]

## Equipe

Gerente(s): @example

Time: @example

Suporte: @example

## Descrição
O projeto tem o objetivo de criar uma API de um CRUD de filmes. Na busca, a API deverá fazer paginação dos filmes para não retornar todos ao mesmo tempo. Deverá ser possível pesquisar um filme por nome e a API deverá retornar todos os filmes que contenham aquele nome. Deverá ser possível incluir, editar e excluir um filme obedecendo os parâmetros exigidos.

## Tipos de Usuário

~"🔧" ~"👤"

## Prazos

de 24 de novembro de 2020 até 05 de dezembro de 2020

## Padrão de branch

Use _{id_da_issue}-descricao-breve_ para nomear as branchs.

**Exemplo**:

-   Para a issue [#1](https://www.notion.so/InfoJrUFBA/projetos/externos/my-time/-/issues/1) use **1-define-usuario** como nome da branch

Obs.: Evite mais de 3 palavras como descrição breve no nome da issue.

## Divisão de Milestones

-   **1º Milestone 2020 nov [24 a 30]**
    - [x] Cadastrar Filme
        - [x] Validar cadastro
    - [x] Listar Filme
    - [x] Editar Filme
    - [x] Remover Filme
-  **2º Milestone 2020 dez [01 a 05]**
    - [x] Implementar paginação
    - [x] Consultar por Id
    - [x] Consultar por Nome
    - [x] Listar todos os filmes
